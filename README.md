# K-anonymity Password Pwned Checker

A simple password pwned checker implementing the k-anoymity model using the haveibeenpwned api as described in this computerphile video: https://youtu.be/hhUb5iknVJs

## Data Source

The source of the data being used to check the password is: [Have I Been Pwned](https://haveibeenpwned.com/)

More specifically, the [haveibeenpwned api](https://haveibeenpwned.com/API/v2)

## Setup

Need to first install the module [`requests`](http://docs.python-requests.org/en/master/user/install/)

1. Clone the public repository:

		$ git clone git://github.com/requests/requests.git

2. Install with pip

		$ cd requests
		$ pip install .

## Run

	$ python passpwnd.py {password}

## Expected Output

> Your password has been pwnd {x} times

## License

Since the api is licensed under CC 4.0, I'll also license this as such

https://creativecommons.org/licenses/by/4.0/