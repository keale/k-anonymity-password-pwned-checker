import sys
import hashlib
import requests

# Using api provided by https://haveibeenpwned.com/API/v2
# Since the api is licensed under CC 4.0, I'll also license this as such
# https://creativecommons.org/licenses/by/4.0/

if len(sys.argv) == 1:
	print("No password provided.")
else:
	password = sys.argv[1]
	#print("password: " , password)
	
	hashed_object = hashlib.sha1(bytes(password, encoding='utf-8'))
	hashed_password = hashed_object.hexdigest().upper()
	#print("hashed_password: " , hashed_password)
	
	prefix = hashed_password[:5]
	#print("prefix: " , prefix)
	
	print("We are using haveibeenpwned.com/API/v2 as the source of the data.")
	
	url = 'https://api.pwnedpasswords.com/range/' + prefix
	headers = {'user-agent': 'k-anonymity-password-checker-in-py/0.0.1'}
	#print("url: " , url)
	
	response = requests.get(url, headers)
	
	#print("response.status_code: ", response.status_code)
	if (response.status_code != 200):
		print("Api responded with status_code: " , response.status_code)
	else:
		#print("response.text:")
		#print(response.text)
		
		pwnd_hash_data_list = response.text.split("\n");
		password_occurence = 0
		
		for pwnd_hash_data in pwnd_hash_data_list:
			suffix = pwnd_hash_data.split(":")[0]
			occurence = pwnd_hash_data.split(":")[1]
			
			pwnd_hash = prefix + suffix
			#print("pwnd_hash: " , pwnd_hash)
			#print("occurence: " , occurence)
			
			if hashed_password == pwnd_hash:
				password_occurence = password_occurence + int(occurence)
				
		print("Your password has been pwnd" , password_occurence , "times")